package com.printsupport.gateway.service;

import com.printsupport.gateway.modal.User;
import com.printsupport.gateway.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User dbUser = this.userRepository.findByUsername(username);

        if (dbUser != null) {
            GrantedAuthority authority = new SimpleGrantedAuthority(dbUser.getRole());

            return new org.springframework.security.core.userdetails.User(
                    dbUser.getUsername(), dbUser.getPassword(), Collections.singletonList(authority));
        } else {
            throw new UsernameNotFoundException(String.format("User '%s' not found", username));
        }
    }

}