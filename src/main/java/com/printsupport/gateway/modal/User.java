package com.printsupport.gateway.modal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;

@Entity
@Table(name="users")
@Builder
@Getter
@Setter
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Column(name = "del_flag")
    private Boolean delFlag = false;

    @JsonIgnore
    @Column(name = "role")
    private String role;

    public User setPasswordToEncode() {
        this.password = new BCryptPasswordEncoder().encode(this.password);
        return this;
    }

    public User() {

    }
}